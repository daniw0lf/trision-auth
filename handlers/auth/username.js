'use strict';

/**
 * Operations on /auth/username/
 */
module.exports = {
    
    /**
     * Use this endpoint to change user username
     * parameters: username, current_password
     * produces: application/json
     */
    post: function POSTSet_Username_POST(req, res) {
        res.sendStatus(501);
    }
    
};
