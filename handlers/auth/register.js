'use strict';

/**
 * Operations on /auth/register/
 */
module.exports = {
    
    /**
     * Use this endpoint to register new user
     * parameters: email, username, password
     * produces: application/json
     */
    post: function POSTRegistration_POST(req, res) {
        res.sendStatus(501);
    }
    
};
