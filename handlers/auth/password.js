'use strict';

/**
 * Operations on /auth/password/
 */
module.exports = {
    
    /**
     * Use this endpoint to change user password
     * parameters: new_password, current_password
     * produces: application/json
     */
    post: function POSTSet_Password_POST(req, res) {
        res.sendStatus(501);
    }
    
};
