'use strict';

/**
 * Operations on /auth/logout/
 */
module.exports = {
    
    /**
     * Use this endpoint to logout user (remove user authentication token)
     * parameters: 
     * produces: application/json
     */
    post: function POSTLogout_POST(req, res) {
        res.sendStatus(501);
    }
    
};
