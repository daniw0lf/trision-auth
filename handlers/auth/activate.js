'use strict';

/**
 * Operations on /auth/activate/
 */
module.exports = {
    
    /**
     * Use this endpoint to activate user account
     * parameters: uid, token
     * produces: application/json
     */
    post: function POSTActivation_POST(req, res) {
        res.sendStatus(501);
    }
    
};
