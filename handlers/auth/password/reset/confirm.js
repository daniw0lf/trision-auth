'use strict';

/**
 * Operations on /auth/password/reset/confirm/
 */
module.exports = {
    
    /**
     * Use this endpoint to finish reset password process
     * parameters: uid, token, new_password
     * produces: application/json
     */
    post: function POSTPassword_Reset_Confirm_POST(req, res) {
        res.sendStatus(501);
    }
    
};
