'use strict';

/**
 * Operations on /auth/password/reset/
 */
module.exports = {
    
    /**
     * Use this endpoint to send email to user with password reset link
     * parameters: email
     * produces: application/json
     */
    post: function POSTPassword_Reset_POST(req, res) {
        res.sendStatus(501);
    }
    
};
