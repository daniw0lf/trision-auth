'use strict';

/**
 * Operations on /auth/login/
 */
module.exports = {
    
    /**
     * Use this endpoint to obtain user authentication token
     * parameters: password
     * produces: application/json
     */
    post: function POSTLogin_POST(req, res) {
        res.sendStatus(501);
    }
    
};
