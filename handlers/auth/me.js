'use strict';

/**
 * Operations on /auth/me/
 */
module.exports = {
    
    /**
     * Use this endpoint to retrieve/update user
     * parameters: 
     * produces: application/json
     */
    get: function GETUser_GET(req, res) {
        res.sendStatus(501);
    }, 
    
    /**
     * Use this endpoint to retrieve/update user
     * parameters: email
     * produces: application/json
     */
    put: function PUTUser_PUT(req, res) {
        res.sendStatus(501);
    }, 
    
    /**
     * Use this endpoint to retrieve/update user
     * parameters: email
     * produces: application/json
     */
    patch: function PATCHUser_PATCH(req, res) {
        res.sendStatus(501);
    }
    
};
