'use strict';

/**
 * Operations on /auth/
 */
module.exports = {
    
    /**
     * Root endpoint - use one of sub endpoints
     * parameters: 
     * produces: application/json
     */
    get: function GETRoot_GET(req, res) {
        res.sendStatus(501);
    }
    
};
