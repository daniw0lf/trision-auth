'use strict';

function SetUsernameSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.username = options.username;
    this.current_password = options.current_password;
}

module.exports = SetUsernameSerializer;
