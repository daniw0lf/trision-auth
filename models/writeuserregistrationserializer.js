'use strict';

function WriteUserRegistrationSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.email = options.email;
    this.username = options.username;
    this.password = options.password;
}

module.exports = WriteUserRegistrationSerializer;
