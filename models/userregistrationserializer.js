'use strict';

function UserRegistrationSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.email = options.email;
    this.username = options.username;
    this.id = options.id;
}

module.exports = UserRegistrationSerializer;
