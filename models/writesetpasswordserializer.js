'use strict';

function WriteSetPasswordSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.new_password = options.new_password;
    this.current_password = options.current_password;
}

module.exports = WriteSetPasswordSerializer;
