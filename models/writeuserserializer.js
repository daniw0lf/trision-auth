'use strict';

function WriteUserSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.email = options.email;
}

module.exports = WriteUserSerializer;
