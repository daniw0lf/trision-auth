'use strict';

function WriteUidAndTokenSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.uid = options.uid;
    this.token = options.token;
}

module.exports = WriteUidAndTokenSerializer;
