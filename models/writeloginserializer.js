'use strict';

function WriteLoginSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.password = options.password;
}

module.exports = WriteLoginSerializer;
