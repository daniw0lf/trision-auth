'use strict';

function WritePasswordResetConfirmSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.uid = options.uid;
    this.token = options.token;
    this.new_password = options.new_password;
}

module.exports = WritePasswordResetConfirmSerializer;
