'use strict';

function UidAndTokenSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.uid = options.uid;
    this.token = options.token;
}

module.exports = UidAndTokenSerializer;
