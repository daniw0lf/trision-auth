'use strict';

function PasswordResetSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.email = options.email;
}

module.exports = PasswordResetSerializer;
