'use strict';

function PasswordResetConfirmSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.uid = options.uid;
    this.token = options.token;
    this.new_password = options.new_password;
}

module.exports = PasswordResetConfirmSerializer;
