'use strict';

function WritePasswordResetSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.email = options.email;
}

module.exports = WritePasswordResetSerializer;
