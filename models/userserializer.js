'use strict';

function UserSerializer(options) {
    if (!options) {
        options = {};
    }
    
    this.email = options.email;
    this.id = options.id;
    this.username = options.username;
}

module.exports = UserSerializer;
