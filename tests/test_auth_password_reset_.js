'use strict';

var test = require('tape'),
    path = require('path'),
    express = require('express'),
    jsYaml = require('js-yaml'),
    fs = require('fs'),
    enjoi = require('enjoi'),
    swaggerize = require('swaggerize-express'),
    request = require('supertest');

test('api', function (t) {
    var app = express();

    
    app.use(require('body-parser')());

    app.use(swaggerize({
        api: path.join(__dirname, './../config/ivan.json'),
        handlers: path.join(__dirname, '../handlers')
    }));

    
    t.test('test post /auth/password/reset/', function (t) {
        
        var responseSchema = enjoi({
            '$ref': "#/definitions/PasswordResetSerializer"
        }, {
          '#':  require(Path.join(__dirname, './../config/ivan.json')) 
        });
        

        request(app).post('//auth/password/reset/')
        .end(function (err, res) {
            t.ok(!err, 'post /auth/password/reset/ no error.');
            t.strictEqual(res.statusCode, 200, 'post /auth/password/reset/ 200 status.');
            responseSchema.validate(res.body, function (error) {
                t.ok(!error, 'Response schema valid.');
            });
            t.end();
        });
    });
    

});
