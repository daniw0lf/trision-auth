'use strict';

var test = require('tape'),
    path = require('path'),
    express = require('express'),
    jsYaml = require('js-yaml'),
    fs = require('fs'),
    enjoi = require('enjoi'),
    swaggerize = require('swaggerize-express'),
    request = require('supertest');

test('api', function (t) {
    var app = express();

    
    app.use(require('body-parser')());

    app.use(swaggerize({
        api: path.join(__dirname, './../config/ivan.json'),
        handlers: path.join(__dirname, '../handlers')
    }));

    
    t.test('test get /auth/me/', function (t) {
        
        var responseSchema = enjoi({
            '$ref': "#/definitions/UserSerializer"
        }, {
          '#':  require(Path.join(__dirname, './../config/ivan.json')) 
        });
        

        request(app).get('//auth/me/')
        .end(function (err, res) {
            t.ok(!err, 'get /auth/me/ no error.');
            t.strictEqual(res.statusCode, 200, 'get /auth/me/ 200 status.');
            responseSchema.validate(res.body, function (error) {
                t.ok(!error, 'Response schema valid.');
            });
            t.end();
        });
    });
    
    t.test('test put /auth/me/', function (t) {
        
        var responseSchema = enjoi({
            '$ref': "#/definitions/UserSerializer"
        }, {
          '#':  require(Path.join(__dirname, './../config/ivan.json')) 
        });
        

        request(app).put('//auth/me/')
        .end(function (err, res) {
            t.ok(!err, 'put /auth/me/ no error.');
            t.strictEqual(res.statusCode, 200, 'put /auth/me/ 200 status.');
            responseSchema.validate(res.body, function (error) {
                t.ok(!error, 'Response schema valid.');
            });
            t.end();
        });
    });
    
    t.test('test patch /auth/me/', function (t) {
        
        var responseSchema = enjoi({
            '$ref': "#/definitions/UserSerializer"
        }, {
          '#':  require(Path.join(__dirname, './../config/ivan.json')) 
        });
        

        request(app).patch('//auth/me/')
        .end(function (err, res) {
            t.ok(!err, 'patch /auth/me/ no error.');
            t.strictEqual(res.statusCode, 200, 'patch /auth/me/ 200 status.');
            responseSchema.validate(res.body, function (error) {
                t.ok(!error, 'Response schema valid.');
            });
            t.end();
        });
    });
    

});
