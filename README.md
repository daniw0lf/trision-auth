# trision2

```json
{
  "swagger": "2.0",
  "info": {
    "version": "1.0.0",
    "title": "auth",
    "license": {
      "name": "MIT",
      "url": "http://github.com/gruntjs/grunt/blob/master/LICENSE-MIT"
    }
  },
  "host": "qamarket.trision3d.com",
  "basePath": "/",
  "securityDefinitions": {},
  "schemes": [
    "http"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/auth/me/": {
      "get": {
        "description": "Use this endpoint to retrieve/update user",
        "operationId": "GETUser_GET",
        "produces": [
          "application/json"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/UserSerializer"
            }
          }
        }
      },
      "put": {
        "description": "Use this endpoint to retrieve/update user",
        "operationId": "PUTUser_PUT",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "formData",
            "required": false,
            "x-is-map": false,
            "type": "string",
            "description": ""
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/UserSerializer"
            }
          }
        }
      },
      "patch": {
        "description": "Use this endpoint to retrieve/update user",
        "operationId": "PATCHUser_PATCH",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "formData",
            "required": false,
            "x-is-map": false,
            "type": "string",
            "description": ""
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/UserSerializer"
            }
          }
        }
      }
    },
    "/auth/register/": {
      "post": {
        "description": "Use this endpoint to register new user",
        "operationId": "POSTRegistration_POST",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "formData",
            "required": false,
            "x-is-map": false,
            "type": "string",
            "description": ""
          },
          {
            "name": "username",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": "Requerido. 30 caracteres o menos. Letras, dígitos y @/./+/-/_ solamente."
          },
          {
            "name": "password",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": ""
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/UserRegistrationSerializer"
            }
          }
        }
      }
    },
    "/auth/activate/": {
      "post": {
        "description": "Use this endpoint to activate user account",
        "operationId": "POSTActivation_POST",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "uid",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": ""
          },
          {
            "name": "token",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": ""
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/UidAndTokenSerializer"
            }
          }
        }
      }
    },
    "/auth/username/": {
      "post": {
        "description": "Use this endpoint to change user username",
        "operationId": "POSTSet_Username_POST",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "username",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": "Requerido. 30 caracteres o menos. Letras, dígitos y @/./+/-/_ solamente."
          },
          {
            "name": "current_password",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": ""
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/SetUsernameSerializer"
            }
          }
        }
      }
    },
    "/auth/password/": {
      "post": {
        "description": "Use this endpoint to change user password",
        "operationId": "POSTSet_Password_POST",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "new_password",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": ""
          },
          {
            "name": "current_password",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": ""
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/SetPasswordSerializer"
            }
          }
        }
      }
    },
    "/auth/password/reset/": {
      "post": {
        "description": "Use this endpoint to send email to user with password reset link",
        "operationId": "POSTPassword_Reset_POST",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": ""
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/PasswordResetSerializer"
            }
          }
        }
      }
    },
    "/auth/password/reset/confirm/": {
      "post": {
        "description": "Use this endpoint to finish reset password process",
        "operationId": "POSTPassword_Reset_Confirm_POST",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "uid",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": ""
          },
          {
            "name": "token",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": ""
          },
          {
            "name": "new_password",
            "in": "formData",
            "required": true,
            "x-is-map": false,
            "type": "string",
            "description": ""
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/PasswordResetConfirmSerializer"
            }
          }
        }
      }
    },
    "/auth/login/": {
      "post": {
        "description": "Use this endpoint to obtain user authentication token",
        "operationId": "POSTLogin_POST",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "password",
            "in": "formData",
            "required": false,
            "x-is-map": false,
            "type": "string",
            "description": ""
          }
        ],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "$ref": "#/definitions/LoginSerializer"
            }
          }
        }
      }
    },
    "/auth/logout/": {
      "post": {
        "description": "Use this endpoint to logout user (remove user authentication token)",
        "operationId": "POSTLogout_POST",
        "produces": [
          "application/json"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "type": "object"
            }
          }
        }
      }
    },
    "/auth/": {
      "get": {
        "description": "Root endpoint - use one of sub endpoints",
        "operationId": "GETRoot_GET",
        "produces": [
          "application/json"
        ],
        "parameters": [],
        "responses": {
          "200": {
            "description": "",
            "schema": {
              "type": "object"
            }
          }
        }
      }
    }
  },
  "definitions": {
    "LoginSerializer": {
      "title": "LoginSerializer",
      "type": "object",
      "properties": {
        "password": {
          "type": "string"
        }
      },
      "required": [
        "password"
      ]
    },
    "UserRegistrationSerializer": {
      "title": "UserRegistrationSerializer",
      "type": "object",
      "properties": {
        "email": {
          "type": "string"
        },
        "username": {
          "description": "Requerido. 30 caracteres o menos. Letras, dígitos y @/./+/-/_ solamente.",
          "type": "string"
        },
        "id": {
          "type": "integer",
          "format": "int64"
        }
      },
      "required": [
        "email",
        "username",
        "id"
      ]
    },
    "PasswordResetConfirmSerializer": {
      "title": "PasswordResetConfirmSerializer",
      "type": "object",
      "properties": {
        "uid": {
          "type": "string"
        },
        "token": {
          "type": "string"
        },
        "new_password": {
          "type": "string"
        }
      },
      "required": [
        "uid",
        "token",
        "new_password"
      ]
    },
    "UserSerializer": {
      "title": "UserSerializer",
      "type": "object",
      "properties": {
        "email": {
          "type": "string"
        },
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "username": {
          "description": "Requerido. 30 caracteres o menos. Letras, dígitos y @/./+/-/_ solamente.",
          "type": "string"
        }
      },
      "required": [
        "email",
        "id",
        "username"
      ]
    },
    "PasswordResetSerializer": {
      "title": "PasswordResetSerializer",
      "type": "object",
      "properties": {
        "email": {
          "type": "string"
        }
      },
      "required": [
        "email"
      ]
    },
    "WriteUserSerializer": {
      "title": "WriteUserSerializer",
      "type": "object",
      "properties": {
        "email": {
          "type": "string"
        }
      }
    },
    "UidAndTokenSerializer": {
      "title": "UidAndTokenSerializer",
      "type": "object",
      "properties": {
        "uid": {
          "type": "string"
        },
        "token": {
          "type": "string"
        }
      },
      "required": [
        "uid",
        "token"
      ]
    },
    "WriteUserRegistrationSerializer": {
      "title": "WriteUserRegistrationSerializer",
      "type": "object",
      "properties": {
        "email": {
          "type": "string"
        },
        "username": {
          "description": "Requerido. 30 caracteres o menos. Letras, dígitos y @/./+/-/_ solamente.",
          "type": "string"
        },
        "password": {
          "type": "string"
        }
      },
      "required": [
        "username",
        "password"
      ]
    },
    "WriteUidAndTokenSerializer": {
      "title": "WriteUidAndTokenSerializer",
      "type": "object",
      "properties": {
        "uid": {
          "type": "string"
        },
        "token": {
          "type": "string"
        }
      },
      "required": [
        "uid",
        "token"
      ]
    },
    "WriteLoginSerializer": {
      "title": "WriteLoginSerializer",
      "type": "object",
      "properties": {
        "password": {
          "type": "string"
        }
      }
    },
    "WritePasswordResetSerializer": {
      "title": "WritePasswordResetSerializer",
      "type": "object",
      "properties": {
        "email": {
          "type": "string"
        }
      },
      "required": [
        "email"
      ]
    },
    "WriteSetUsernameSerializer": {
      "title": "WriteSetUsernameSerializer",
      "type": "object",
      "properties": {
        "username": {
          "description": "Requerido. 30 caracteres o menos. Letras, dígitos y @/./+/-/_ solamente.",
          "type": "string"
        },
        "current_password": {
          "type": "string"
        }
      },
      "required": [
        "username",
        "current_password"
      ]
    },
    "WriteSetPasswordSerializer": {
      "title": "WriteSetPasswordSerializer",
      "type": "object",
      "properties": {
        "new_password": {
          "type": "string"
        },
        "current_password": {
          "type": "string"
        }
      },
      "required": [
        "new_password",
        "current_password"
      ]
    },
    "WritePasswordResetConfirmSerializer": {
      "title": "WritePasswordResetConfirmSerializer",
      "type": "object",
      "properties": {
        "uid": {
          "type": "string"
        },
        "token": {
          "type": "string"
        },
        "new_password": {
          "type": "string"
        }
      },
      "required": [
        "uid",
        "token",
        "new_password"
      ]
    },
    "SetPasswordSerializer": {
      "title": "SetPasswordSerializer",
      "type": "object",
      "properties": {
        "new_password": {
          "type": "string"
        },
        "current_password": {
          "type": "string"
        }
      },
      "required": [
        "new_password",
        "current_password"
      ]
    },
    "SetUsernameSerializer": {
      "title": "SetUsernameSerializer",
      "type": "object",
      "properties": {
        "username": {
          "description": "Requerido. 30 caracteres o menos. Letras, dígitos y @/./+/-/_ solamente.",
          "type": "string"
        },
        "current_password": {
          "type": "string"
        }
      },
      "required": [
        "username",
        "current_password"
      ]
    }
  }
}
```
